/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package empresaagroalimentaria;

/**1-EmpresaAgroalimentaria
 * fechaCaducidad
 * numeroLote
 * nombre producto (nombre)
 * 
 *      -11-Producto frescos(Frescos)
 *      fechaEnvasado
 *      paisOrigen
 *      imprimirFrescos()
 * 
 *      -12-Productos refrigerados(Refrigerados)
 *      codigoSupervision
 *      fechaEnvasado
 *      temperaturaMantenimiento
 *      paisOrigen
 *      imprimirRefrigerados()
 * 
 *      -13-Prod congelados(Congelados)
 *      fechaEnvasado
 *      paisOrigen
 *      temperaturaMantenimeinto
 *      imprimirCongelados()
 * 
 *          -131-Por aire(CongeladosPorAire)
 *          %O2 (oxigeno)
 *          %N2 (nitrogeno)
 *          %CO2 (dioxidoCarbono)
 *          %Vapor H20 (vaporAgua)
 *          imprimirCongeladosPorAire()
 *          -132-Por agua(CongeladosPorAgua)
 *          salinidadAgua
 *          imprimirCongeladosPorAgua()
 *          -133-Por nitrogeno(CongeladosPorNitrogeno)
 *          metodo congelacion(metodoCongelacion)
 *          tiempo de exposicion al N2 en segundos (exposicionNitrogeno)
 *          imprimirCongeladosPorNitrogeno()
 * 
 */

public class EmpresaAgroalimentaria {

    public String fechaCaducidad;
    public int numeroLote;
    public String nombre; //nombre producto

    public EmpresaAgroalimentaria(String fechaCaducidad, int numeroLote, String nombre) {
        this.fechaCaducidad = fechaCaducidad;
        this.numeroLote = numeroLote;
        this.nombre = nombre;
    }
    
    public String getFechaCaducidad() {
        return fechaCaducidad;
    }

    public void setFechaCaducidad(String fechaCaducidad) {
        this.fechaCaducidad = fechaCaducidad;
    }

    public int getNumeroLote() {
        return numeroLote;
    }

    public void setNumeroLote(int numeroLote) {
        this.numeroLote = numeroLote;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public void imprimir(){
        System.out.println("\nNombre producto: " + nombre + "\tFecha caducidad: " + fechaCaducidad + "\tNumeroLote: " + numeroLote);
    }
    
}
