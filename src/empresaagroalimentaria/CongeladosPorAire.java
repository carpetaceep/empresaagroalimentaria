/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package empresaagroalimentaria;

public class CongeladosPorAire extends Congelados{
    //% de cada cosa
    public int oxigeno;
    public int nitrogeno;
    public int dioxidoCarbono;
    public int vaporAgua;

    public CongeladosPorAire(int oxigeno, int nitrogeno, int dioxidoCarbono, int vaporAgua, String fechaEnvasado, String paisOrigen, int temperaturaMantenimiento, String fechaCaducidad, int numeroLote, String nombre) {
        super(fechaEnvasado, paisOrigen, temperaturaMantenimiento, fechaCaducidad, numeroLote, nombre);
        this.oxigeno = oxigeno;
        this.nitrogeno = nitrogeno;
        this.dioxidoCarbono = dioxidoCarbono;
        this.vaporAgua = vaporAgua;
    }

    public int getOxigeno() {
        return oxigeno;
    }

    public void setOxigeno(int oxigeno) {
        this.oxigeno = oxigeno;
    }

    public int getNitrogeno() {
        return nitrogeno;
    }

    public void setNitrogeno(int nitrogeno) {
        this.nitrogeno = nitrogeno;
    }

    public int getDioxidoCarbono() {
        return dioxidoCarbono;
    }

    public void setDioxidoCarbono(int dioxidoCarbono) {
        this.dioxidoCarbono = dioxidoCarbono;
    }

    public int getVaporAgua() {
        return vaporAgua;
    }

    public void setVaporAgua(int vaporAgua) {
        this.vaporAgua = vaporAgua;
    }
    
    

    void imprimirCongeladosPorAire(){
        imprimir();
        imprimirCongelados();
        System.out.println("% de Oxígeno: " + oxigeno + "\t% de Nitrógeno: " + nitrogeno + "\t% de Dioxido de carbono: " + dioxidoCarbono + "\t% de Vapor de agua: " + vaporAgua);
    }
}
