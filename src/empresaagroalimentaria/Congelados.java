/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package empresaagroalimentaria;

/**
 *
 * @author Jenesepados
 */
public class Congelados extends EmpresaAgroalimentaria{
    public String fechaEnvasado;
    public String paisOrigen;
    public int temperaturaMantenimiento;

    public Congelados(String fechaEnvasado, String paisOrigen, int temperaturaMantenimiento, String fechaCaducidad, int numeroLote, String nombre) {
        super(fechaCaducidad, numeroLote, nombre);
        this.fechaEnvasado = fechaEnvasado;
        this.paisOrigen = paisOrigen;
        this.temperaturaMantenimiento = temperaturaMantenimiento;
    }

    public String getFechaEnvasado() {
        return fechaEnvasado;
    }

    public void setFechaEnvasado(String fechaEnvasado) {
        this.fechaEnvasado = fechaEnvasado;
    }

    public String getPaisOrigen() {
        return paisOrigen;
    }

    public void setPaisOrigen(String paisOrigen) {
        this.paisOrigen = paisOrigen;
    }

    public int getTemperaturaMantenimiento() {
        return temperaturaMantenimiento;
    }

    public void setTemperaturaMantenimiento(int temperaturaMantenimiento) {
        this.temperaturaMantenimiento = temperaturaMantenimiento;
    }
    
    
    
    void imprimirCongelados(){
        imprimir();
        System.out.println("Fecha envasado: " + fechaEnvasado + "\tPais origen: " + paisOrigen + "\tTemperatura mantenimiento: " + temperaturaMantenimiento);
    }
}
