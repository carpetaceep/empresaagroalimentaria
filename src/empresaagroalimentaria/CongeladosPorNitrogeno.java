/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package empresaagroalimentaria;

public class CongeladosPorNitrogeno extends Congelados{
    public String metodoCongelacion = "Por Nitrogeno";
    public int exposicionNitrogeno; //Tiempo en segundos de expo

    public CongeladosPorNitrogeno(int exposicionNitrogeno, String fechaEnvasado, String paisOrigen, int temperaturaMantenimiento, String fechaCaducidad, int numeroLote, String nombre) {
        super(fechaEnvasado, paisOrigen, temperaturaMantenimiento, fechaCaducidad, numeroLote, nombre);
        this.exposicionNitrogeno = exposicionNitrogeno;
    }

    public String getMetodoCongelacion() {
        return metodoCongelacion;
    }

    public void setMetodoCongelacion(String metodoCongelacion) {
        this.metodoCongelacion = metodoCongelacion;
    }

    public int getExposicionNitrogeno() {
        return exposicionNitrogeno;
    }

    public void setExposicionNitrogeno(int exposicionNitrogeno) {
        this.exposicionNitrogeno = exposicionNitrogeno;
    }
    
    void imprimirCongeladosPorNitrogeno(){
        imprimir();
        imprimirCongelados();
        System.out.println("Método de congelación: " + metodoCongelacion + "\tTiempo de exposición: " + exposicionNitrogeno);
    }
}
