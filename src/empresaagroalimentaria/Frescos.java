package empresaagroalimentaria;

public class Frescos extends EmpresaAgroalimentaria{
    public String fechaEnvasado;
    public String paisOrigen;

    public Frescos(String fechaEnvasado, String paisOrigen, String fechaCaducidad, int numeroLote, String nombre) {
        super(fechaCaducidad, numeroLote, nombre);
        this.fechaEnvasado = fechaEnvasado;
        this.paisOrigen = paisOrigen;
    }

    public String getFechaEnvasado() {
        return fechaEnvasado;
    }

    public void setFechaEnvasado(String fechaEnvasado) {
        this.fechaEnvasado = fechaEnvasado;
    }

    public String getPaisOrigen() {
        return paisOrigen;
    }

    public void setPaisOrigen(String paisOrigen) {
        this.paisOrigen = paisOrigen;
    }
    
    void imprimirFrescos(){
        imprimir();
        System.out.println("Fecha envasado: " + fechaEnvasado + "\tPais origen: " + paisOrigen);
    }
}
