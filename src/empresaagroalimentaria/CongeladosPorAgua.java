/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package empresaagroalimentaria;

public class CongeladosPorAgua extends Congelados{
    public int salinidadAgua;

    public CongeladosPorAgua(int salinidadAgua, String fechaEnvasado, String paisOrigen, int temperaturaMantenimiento, String fechaCaducidad, int numeroLote, String nombre) {
        super(fechaEnvasado, paisOrigen, temperaturaMantenimiento, fechaCaducidad, numeroLote, nombre);
        this.salinidadAgua = salinidadAgua;
    }

    public int getSalinidadAgua() {
        return salinidadAgua;
    }

    public void setSalinidadAgua(int salinidadAgua) {
        this.salinidadAgua = salinidadAgua;
    }
    
    void imprimirCongeladosPorAgua(){
        imprimir();
        imprimirCongelados();
        System.out.println("Salinidad Agua: " + salinidadAgua);
    }
}
