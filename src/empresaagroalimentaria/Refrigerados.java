
package empresaagroalimentaria;

public class Refrigerados extends EmpresaAgroalimentaria{
    public int codigoSupervision;
    public String fechaEnvasado;
    public int temperaturaMantenimiento;
    public String paisOrigen;

    public Refrigerados(int codigoSupervision, String fechaEnvasado, int temperaturaMantenimiento, String paisOrigen, String fechaCaducidad, int numeroLote, String nombre) {
        super(fechaCaducidad, numeroLote, nombre);
        this.codigoSupervision = codigoSupervision;
        this.fechaEnvasado = fechaEnvasado;
        this.temperaturaMantenimiento = temperaturaMantenimiento;
        this.paisOrigen = paisOrigen;
    }

    public int getCodigoSupervision() {
        return codigoSupervision;
    }

    public void setCodigoSupervision(int codigoSupervision) {
        this.codigoSupervision = codigoSupervision;
    }

    public String getFechaEnvasado() {
        return fechaEnvasado;
    }

    public void setFechaEnvasado(String fechaEnvasado) {
        this.fechaEnvasado = fechaEnvasado;
    }

    public int getTemperaturaMantenimiento() {
        return temperaturaMantenimiento;
    }

    public void setTemperaturaMantenimiento(int temperaturaMantenimiento) {
        this.temperaturaMantenimiento = temperaturaMantenimiento;
    }

    public String getPaisOrigen() {
        return paisOrigen;
    }

    public void setPaisOrigen(String paisOrigen) {
        this.paisOrigen = paisOrigen;
    }
    
    
    
    void imprimirRefrigerados(){
        imprimir();
        System.out.println("Codigo supervision: " + codigoSupervision + "\tFecha Envasado: " + fechaEnvasado + "\tTemperatura Mantenimiento: " + temperaturaMantenimiento + "\tPais origen: " + paisOrigen);
    }
}
